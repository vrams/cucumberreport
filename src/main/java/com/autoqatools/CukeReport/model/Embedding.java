package com.autoqatools.CukeReport.model;

/**
 * Created by vthaduri on 17/10/2016.
 */
public class Embedding
{
    public String getMime_type() {
        return mime_type;
    }

    public void setMime_type(String mime_type) {
        this.mime_type = mime_type;
    }

    public String mime_type;
    public String data;
}
