package com.autoqatools.CukeReport.model;

import java.util.List;

/**
 * Created by vthaduri on 17/10/2016.
 */
public class CucumberJsonWrapper {

    private List<CucumberJson> cucumberJsons;

    public List<CucumberJson> getCucumberJsons() {
        return cucumberJsons;
    }

    public void setCucumberJsons(List<CucumberJson> cucumberJsons) {
        this.cucumberJsons = cucumberJsons;
    }
}
