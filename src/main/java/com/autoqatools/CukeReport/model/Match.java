package com.autoqatools.CukeReport.model;

/**
 * Created by vthaduri on 17/10/2016.
 */
public class Match
{
    private String location;
    private Object arguments;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Object getArguments() {
        return arguments;
    }

    public void setArguments(Object arguments) {
        this.arguments = arguments;
    }
}
