package com.autoqatools.CukeReport.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by vthaduri on 17/10/2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Element
{
    public List<After> after;
    public String id;
    public List<Tag> tags;
    public List<Before> before;
    public String description;
    public String name;
    public String keyword;
    public int line;
    public List<Step> steps;
    public String type;

    public List<Before> getBefore() {
        return before;
    }

    public void setBefore(List<Before> before) {
        this.before = before;
    }

    public List<After> getAfter() {
        return after;
    }

    public void setAfter(List<After> after) {
        this.after = after;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
