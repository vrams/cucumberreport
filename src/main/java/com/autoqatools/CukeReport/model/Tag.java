package com.autoqatools.CukeReport.model;

/**
 * Created by vthaduri on 17/10/2016.
 */
public class Tag
{
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public String name;
    public int line;
}
