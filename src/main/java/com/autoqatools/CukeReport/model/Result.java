package com.autoqatools.CukeReport.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by vthaduri on 17/10/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Result
{
    public long duration;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String status;

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
}
