package com.autoqatools.CukeReport.model;

/**
 * Created by vthaduri on 17/10/2016.
 */
public class Before {
    public Result result;
    public Match match;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }
}
