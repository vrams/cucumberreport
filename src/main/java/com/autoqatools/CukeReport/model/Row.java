package com.autoqatools.CukeReport.model;

import java.util.List;

/**
 * Created by vthaduri on 17/10/2016.
 */
public class Row
{
    public List<String> getCells() {
        return cells;
    }

    public void setCells(List<String> cells) {
        this.cells = cells;
    }

    public List<String> cells;
    public int line;
}
