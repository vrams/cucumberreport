package com.autoqatools.CukeReport.service;

import com.autoqatools.CukeReport.model.CucumberJson;
import com.autoqatools.CukeReport.model.Step;
import com.autoqatools.CukeReport.model.Tag;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by vthaduri on 17/10/2016.
 */
public class GenerateCukeReport {
    private ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    private List<CucumberJson> cucumberJsons = new ArrayList<CucumberJson>();// = new ArrayList<CucumberJson>();
    private File file = new File("C:\\Users\\vthaduri\\WebService\\CukeReport\\src\\main\\java\\com\\autoqatools\\CukeReport\\service\\cucumber.json");

    public GenerateCukeReport() {
        try {
            cucumberJsons = objectMapper.readValue(file,
                    objectMapper.getTypeFactory().constructCollectionType(List.class, CucumberJson.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getFeatures() {
        return cucumberJsons.size();
    }

    public String getFeatureName(int featureIndex) {
        return cucumberJsons.get(featureIndex).getId();
    }

    public int getScenarios(int featureIndex) {
        if (cucumberJsons.get(featureIndex).getElements() == null)
            return 0;
        else
            return cucumberJsons.get(featureIndex).getElements().size();
    }

    public int getSteps(int featureIndex) {
        int count = 0;
        for (int i = 0; i < getScenarios(featureIndex); i++) {
            count = count + cucumberJsons.get(featureIndex).getElements().get(i).getSteps().size();
        }
        return count;
    }

    public int getStepsByStatus(int featureIndex, String status) {
        int count = 0;
        for (int i = 0; i < getScenarios(featureIndex); i++) {
            int steps = cucumberJsons.get(featureIndex).getElements().get(i).getSteps().size();
            for (int j = 0; j < steps; j++) {
                try {
                    if (cucumberJsons.get(featureIndex).getElements().get(i).getSteps().get(j).getResult().
                            getStatus().equalsIgnoreCase(status))
                        count++;
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        }
        return count;
    }

    public long getDuration(int featureIndex) {
        long duration = 0L;
        for (int i = 0; i < getScenarios(featureIndex); i++) {
            try {
                for (int j = 0; j < getSteps(featureIndex); j++) {
                    duration = duration + cucumberJsons.get(featureIndex).getElements().get(i).getSteps().get(j).
                            getResult().getDuration();
                }
            } catch (Exception e) {
                e.getMessage();
            }
        }
        return duration;
    }

    private static String convertSecondToString(long secondTime) {
        secondTime = TimeUnit.NANOSECONDS.toSeconds(secondTime);

        TimeZone tz = TimeZone.getTimeZone("UTC");
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        df.setTimeZone(tz);
        String[] time = df.format(new Date(secondTime * 1000L)).split(":");
        String returnTime = "";
        if (time[0].equalsIgnoreCase("00"))
            returnTime = returnTime.trim() + " 0 Hrs ";
        else
            returnTime = returnTime + String.valueOf(Integer.parseInt(time[0])) + " Hrs ";
        if (time[1].equalsIgnoreCase("00"))
            returnTime = returnTime.trim() + " 0 Mins ";
        else
            returnTime = returnTime + String.valueOf(Integer.parseInt(time[1])) + " Mins ";
        if (time[2].equalsIgnoreCase("00"))
            returnTime = returnTime.trim() + " 0 Secs ";
        else
            returnTime = returnTime + String.valueOf(Integer.parseInt(time[2])) + " Secs ";
        return returnTime;
    }

    public Map<String, List<Long>> getFeatureStatistics() {
        Map<String, List<Long>> map = new HashMap<String, List<Long>>();
        try {
            //For each feature file
            for (int i = 0; i < getFeatures(); i++)
            {
                //For each scenario
                for (int j = 0; j < getScenarios(i); j++)
                {
                    //Get the steps for each scenario
                    List<Step> step = new ArrayList<Step>();
                    step = cucumberJsons.get(i).getElements().get(j).getSteps();

                    //Get the tags listed for each scenario
                    List<Tag> tags = cucumberJsons.get(i).getElements().get(j).getTags();

                    if (tags != null)
                    {
                        //Iterate over each tag.
                        for (int k = 0; k < tags.size(); k++)
                        {
                            if (!map.containsKey(tags.get(k).getName())) {
                                //Create a list for each tag
                                List<Long> statistics = new ArrayList<Long>();
                                for (int a = 0; a < 8; a++) {
                                    statistics.add(0L);
                                }
                                map.put(tags.get(k).getName(), statistics);
                            }
                            int passed = 0;int failed = 0; int skipped = 0; int pending = 0;
                            //Get the step statistics for each step.
                            boolean scenarioFailed = false;
                            long duration = 0;
                            for (int m = 0; m < step.size(); m++)
                            {
                                if (step.get(m).getResult().getStatus().equalsIgnoreCase("passed"))
                                    map.get(tags.get(k).getName()).set(3, map.get(tags.get(k).getName()).get(3) + 1);
                                else if (step.get(m).getResult().getStatus().equalsIgnoreCase("failed"))
                                {
                                    map.get(tags.get(k).getName()).set(4, map.get(tags.get(k).getName()).get(4) + 1);
                                    scenarioFailed = true;
                                }
                                else if (step.get(m).getResult().getStatus().equalsIgnoreCase("skipped"))
                                    map.get(tags.get(k).getName()).set(5, map.get(tags.get(k).getName()).get(6) + 1);
                                else if (step.get(m).getResult().getStatus().equalsIgnoreCase("pending"))
                                    map.get(tags.get(k).getName()).set(6, map.get(tags.get(k).getName()).get(6) + 1);
                                duration = step.get(m).getResult().getDuration();
                                map.get(tags.get(k).getName()).set(7, map.get(tags.get(k).getName()).get(7) + duration);

                            }
                            if(scenarioFailed)
                                map.get(tags.get(k).getName()).set(0, map.get(tags.get(k).getName()).get(0) + 1);
                            else
                                map.get(tags.get(k).getName()).set(1, map.get(tags.get(k).getName()).get(1) + 1);

                            map.get(tags.get(k).getName()).set(2, map.get(tags.get(k).getName()).get(0) +
                                    map.get(tags.get(k).getName()).get(1));

                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    public static void main(String args[]) {
        GenerateCukeReport generateCukeReport = new GenerateCukeReport();
        System.out.println("Feature Statistics");
        for (int i = 0; i < generateCukeReport.getFeatures(); i++) {
            System.out.print(generateCukeReport.getFeatureName(i));
            System.out.print(" " + generateCukeReport.getScenarios(i));
            System.out.print(" " + generateCukeReport.getSteps(i));
            System.out.print(" " + generateCukeReport.getStepsByStatus(i, "passed"));
            System.out.print(" " + generateCukeReport.getStepsByStatus(i, "failed"));
            System.out.print(" " + generateCukeReport.getStepsByStatus(i, "skipped"));
            System.out.print(" " + convertSecondToString(generateCukeReport.getDuration(i)));
            System.out.println();
        }
        System.out.println("Tag Statistics");
        System.out.println(generateCukeReport.getFeatureStatistics().toString());
    }
}

