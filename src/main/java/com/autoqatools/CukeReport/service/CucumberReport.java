package com.autoqatools.CukeReport.service;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by vthaduri on 14/06/2016.
 */
public class CucumberReport
{
    private ObjectMapper objectMapper;
    private JsonNode jsonFeatures;
    private StringBuilder featureStatistics;
    private ArrayList<String> tableRow;
    private File file;
    private Map<String,ArrayList<String>> statistics = new HashMap<String, ArrayList<String>>();
    private Map<String,ArrayList<Integer>> tagOverview = new HashMap<String, ArrayList<Integer>>();

    public CucumberReport()
    {
        ClassLoader classLoader = getClass().getClassLoader();
        file = new File(classLoader.getResource("cucumber.json").getFile());
        try
        {
            objectMapper = new ObjectMapper();
            jsonFeatures = objectMapper.readTree(file);
        }
        catch (Exception e)
        {
            e.getStackTrace();
        }
    }

    private String convertSecondToString(int secondTime)
    {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        df.setTimeZone(tz);
        String[] time = df.format(new Date(secondTime*1000L)).split(":");
        String returnTime = "";
        if(time[0].equalsIgnoreCase("00"))
            returnTime = returnTime.trim() + " 0 Hrs ";
        else
            returnTime = returnTime + String.valueOf(Integer.parseInt(time[0])) + " Hrs ";
        if(time[1].equalsIgnoreCase("00"))
            returnTime = returnTime.trim() + " 0 Mins ";
        else
            returnTime = returnTime + String.valueOf(Integer.parseInt(time[1])) + " Mins ";
        if(time[2].equalsIgnoreCase("00"))
            returnTime = returnTime.trim() + " 0 Secs ";
        else
            returnTime = returnTime + String.valueOf(Integer.parseInt(time[2])) + " Secs ";
        return returnTime;
    }

    public void getReport()
    {

        ArrayList<Integer> tagList = null;

        int steps = 0;
        int passed = 0;
        int failed = 0;
        int skipped = 0;
        int pending = 0;
        int undefined = 0;
        int missing = 0;
        int scenarioPassed = 0;
        int scenarioFailed = 0;
        double duration = 0;
        int noOfSteps = 0;
        String status = "";

        int noOfFeatures = jsonFeatures.size();
        //For each feature file (Story file)
        for(int i = 0; i < noOfFeatures; i++)
        {
            String featureName = jsonFeatures.get(i).get("id").asText();
            JsonNode feature = jsonFeatures.get(i);
            JsonNode featureScenarios = feature.get("elements");
            //For each Scenario in feature file
            for(int j = 0; j < featureScenarios.size(); j++)
            {
                JsonNode scenarioSteps = featureScenarios.get(j).get("steps");
                steps = steps + scenarioSteps.size();
                //For each step in scenario
                for(int l = 0;l < scenarioSteps.size();l++)
                {
                    noOfSteps++;
                    JsonNode stepsResult = scenarioSteps.get(l).get("result");
                    status = stepsResult.get("status").asText();
                    if (stepsResult.get("status").asText().equalsIgnoreCase("passed"))
                        passed++;
                    if (stepsResult.get("status").asText().equalsIgnoreCase("failed"))
                        failed++;
                    if (stepsResult.get("status").asText().equalsIgnoreCase("skipped"))
                        skipped++;
                    if (stepsResult.get("status").asText().equalsIgnoreCase("pending"))
                        pending++;
                    if (stepsResult.get("status").asText().equalsIgnoreCase("undefined"))
                        undefined++;
                    if (stepsResult.get("status").asText().equalsIgnoreCase("missing"))
                        missing++;
                    if(!(stepsResult.get("status").asText().equalsIgnoreCase("pending")|stepsResult.get("status").asText().equalsIgnoreCase("skipped")))
                    duration = duration + (float)stepsResult.get("duration").asLong()/1000000000;
                    JsonNode tags = featureScenarios.get(j).get("tags");
                    for (int m = 0; m < tags.size(); m++)
                    {
                        if(tagOverview.containsKey(tags.get(m).get("name").asText()))
                        {
                            tagList = new ArrayList<Integer>();
                            tagList = tagOverview.get(tags.get(m).get("name").asText());
                            tagOverview.get(tags.get(m).get("name").asText()).clear();
                            tagList.add(0,noOfSteps);
                            tagList.add(1,passed);
                            tagList.add(2,failed);
                            tagList.add(3,skipped);
                            tagList.add(4,pending);
                            tagList.add(5,undefined);
                            tagList.add(6,missing);
                            tagOverview.put(tags.get(m).get("name").asText(),tagList);
                            tagList.add(7,(int)Math.round(duration*100)/100);
                        }
                        else
                        {
                            tagList = new ArrayList<Integer>();
                            tagList.add(0,noOfSteps);
                            tagList.add(1,passed);
                            tagList.add(2,failed);
                            tagList.add(3,skipped);
                            tagList.add(4,pending);
                            tagList.add(5,undefined);
                            tagList.add(6,missing);
                            tagOverview.put(tags.get(m).get("name").asText(),tagList);
                            tagList.add(7,(int)Math.round(duration*100)/100);
                        }
                    }
                }
                if(status.equalsIgnoreCase("passed"))
                    scenarioPassed++;
                else
                    scenarioFailed++;
            }
            tableRow = new ArrayList<String>();
            tableRow.add(Integer.toString(featureScenarios.size()));
            tableRow.add(Integer.toString(scenarioPassed));
            tableRow.add(Integer.toString(scenarioFailed));
            tableRow.add(Integer.toString(passed));
            tableRow.add(Integer.toString(failed));
            tableRow.add(Integer.toString(skipped));
            tableRow.add(Integer.toString(pending));
            tableRow.add(Integer.toString(undefined));
            tableRow.add(Integer.toString(missing));
            tableRow.add(convertSecondToString((int) (Math.round(duration*100)/100)));
            tableRow.add(String.valueOf (Math.round(duration*100)/100));
            statistics.put(featureName,tableRow);
            duration = 0;
            scenarioPassed = scenarioFailed = 0;
            status = "";
        }
        for (Map.Entry<String,ArrayList<String>> entry : statistics.entrySet())
        {
            String key = entry.getKey();
            System.out.print(key + " ");
            for(String value : entry.getValue())
            {
                System.out.print(value + " ");
            }
            System.out.println();
        }
        for (Map.Entry<String,ArrayList<Integer>> tags: tagOverview.entrySet())
        {
            String key = tags.getKey();
            System.out.print(key + " ");
            for(int value : tags.getValue())
            {
                System.out.print(value + " ");
            }
            System.out.println();
        }

    }

    //Returns number of features
    public int getFeatures()
    {
        return jsonFeatures.size();
    }

    //Returns the Feature name by given index of json node
    public String getFeatureNameByIndex(int featureIndex)
    {
        String featureName = "";
        try
        {
            featureName = jsonFeatures.get(featureIndex).get("id").asText();
        }
        catch (NullPointerException e)
        {
            System.out.println("Error: No Feature present at the index:" + featureIndex);
            e.printStackTrace();
        }
        return featureName;
    }

    //Returns the number of scenarios in the feature file
    public int getScenarios(int featureIndex)
    {
        JsonNode scenarios = jsonFeatures.get(featureIndex);
        return scenarios.get("elements").size();
    }

    public int getScenariosByStatus(int featureIndex, String status)
    {
        int count = 0;
        try
        {
            JsonNode feature = jsonFeatures.get(featureIndex);
            JsonNode scenarios = feature.get("elements");
            for (int i = 0; i < scenarios.size(); i++) {
                JsonNode scenario = scenarios.get(i).get("after");
                if (scenarios.get(i).get("after").get(0).get("result").get("status").asText().equalsIgnoreCase(status)) {
                    count++;
                }
            }
        }
        catch (NullPointerException e)
        {
            System.out.println("Error: No Feature or Scenario present at the index:" + featureIndex);
            e.printStackTrace();
        }
        return count;
    }

    public int getStepsByStatus(int featureIndex, String status)
    {
        int count = 0;
        try
        {

        }
        catch (NullPointerException e)
        {
            System.out.println("Error: No Feature or Step present at the index:" + featureIndex);
            e.printStackTrace();
        }
        return count;
    }
    public static void main(String args[])
    {
        CucumberReport cucumberReport = new CucumberReport();
        cucumberReport.getReport();
        //System.out.println("Number of features:"+cucumberReport.getScenariosByStatus(0,"failed"));
        /*for (int i = 0; i< cucumberReport.getFeatures();i++)
        {
            System.out.println(cucumberReport.getFeatureNameByIndex(i)
                    + ":" + cucumberReport.getScenarios(i)
                    + "-" + cucumberReport.getScenariosByStatus(i,"passed")
                    + "-" + cucumberReport.getScenariosByStatus(i,"failed")
                    );
        }*/
    }
}

